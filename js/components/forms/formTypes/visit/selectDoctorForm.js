import Form from "../form.js";
import {VisitCardioForm, VisitDentistForm, VisitTherapistForm} from "./visitTypes/index.js";
import {Select} from "../../formFields/index.js";
import {render} from "../../../../functions/index.js";

class SelectDoctorForm extends Form {
    constructor({id, displayType = "", className = ""}) {
        super({id, displayType, className});
    }

    render(){
        this.elem = super.render();

        const selectProps = {
            name: "text",
            label:"Выберите врача",
            options:[{value:"cardiologist", text:"Кардиолог"}, {value:"dentist", text:"Стоматолог"}, {value:"therapist", text:"Терапевт"}]
        };
        this.select = new Select(selectProps);
        render(this.select, this.elem);

        this.select.elem.addEventListener('change', this.selectDoctor.bind(this));
        return this.elem
    }

    selectDoctor(){
        const doctorType = this.select.elem.value;
        let form = "";
        if (doctorType === "cardiologist"){
            form = new VisitCardioForm();
        } else if (doctorType === "dentist"){
            form = new VisitDentistForm();
        }else if (doctorType === "therapist"){
            form = new VisitTherapistForm();
        }
        render(form, this.elem);
    }
}

export {SelectDoctorForm};