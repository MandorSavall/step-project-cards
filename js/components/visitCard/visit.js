class Visit {
    constructor({purpose, description, urgency, lastName, name, middleName, id}) {
        this.purpose = purpose;
        this.description = description;
        this.urgency = urgency;
        this.lastName = lastName;
        this.name = name;
        this.middleName = middleName;
        this.className = 'card mb-4 box-shadow';
        this.cardId = id;
        this.elem = null;
    }

    render() {
        this.elem = document.createElement('div');
        this.elem.className = this.className;

        this.elem.insertAdjacentHTML('beforeend', `<span class="delete-card">X</span><div class="card-body">
            <h4 class="card-title patient-name">ФИО: ${this.lastName} ${this.name} ${this.middleName} </h4>
            <button type="button" class="btn btn-lg btn-block btn-outline-primary btn-show-more">Подробнее</button>
            </div>`);
        const deleteCard = this.elem.querySelector('.delete-card');
        deleteCard.addEventListener('click', this.delete.bind(this));
        const btnShow = this.elem.querySelector('.btn-show-more');
        btnShow.addEventListener('click', this.showMore.bind(this));

        this.elem.addEventListener('dragstart', this.dragStart.bind(this));
        this.elem.addEventListener('drop', this.drop.bind(this));

    }


    showMore(e){
    e.preventDefault();
        e.target.insertAdjacentHTML('beforebegin', `   <ul class="list-unstyled mt-3 mb-4 adition-info">
                                                                     <li>Причина обращения:${this.purpose}</li>
                                                                     <li>Краткое описание визита:${this.description}</li>
                                                                     <li>Срочность:${this.urgency}</li>
                                                                     </ul>`)
    }

    async delete(){
        const {data} = await axios.delete(`http://cards.danit.com.ua/cards/${this.cardId}`);
        if(data.status === 'Success'){
            this.elem.remove();
        }
    }
    
    edit(){
        
    }

    drop(e) {
        e.preventDefault();
        let {target} = e;
        if(!e.target.classList.contains('.card')) {
            target = target.closest('.card');
        }
        if(!target){
            let {patientX, patientY} = e;
            patientY += 5;
            let elementMouseIsOver = document.elementFromPoint(patientX, patientY);
            while(!elementMouseIsOver.classList.contains('card')) {
                patientY += 5;
                elementMouseIsOver = document.elementFromPoint(patientX, patientY);
            }
            target = elementMouseIsOver;
        }
        target.before(this.dragElem);
        this.dragElem = null;
    }

    dragStart() {
        this.dragElem = this.elem;
    }
}

export default Visit;