import {createElement} from "../../functions/index.js";

class Modal {
    constructor({id, modalHeading}) {
      this.attr = {id, className: "modal fade"};
      this.modalHeading = modalHeading;
    }
    
    render(){
        const elem = createElement("div", this.attr);
        elem.insertAdjacentHTML("beforeend", `<div class="modal-dialog">
        <div class="modal-content">
    
          <div class="modal-header">
            <h4 class="modal-title">${this.modalHeading}</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
    
          <div class="modal-body">
          </div>
    
          <div class="modal-footer">
            <button type="button" class="btn btn-danger close-btn" data-dismiss="modal">Close</button>
          </div>
    
        </div>
      </div>`);
      
        const closeButtons = elem.querySelectorAll(".close, .close-btn");
        closeButtons.forEach(button => button.addEventListener("click", this.close.bind(this)));
        return elem;
    }
    
    open(e) {
        e.preventDefault();
        this.elem.classList.add("show");
    }
    
    close() {
        this.elem.classList.remove("show");
    }
}

export {Modal};