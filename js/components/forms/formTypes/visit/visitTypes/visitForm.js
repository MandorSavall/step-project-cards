import Form from '../../form.js'
import {Input, Textarea, Select} from "../../../formFields/index.js";

class VisitForm extends Form {
    constructor({purposeOfTheVisit, briefDescriptionOfTheVisit, urgency, age, surname, name,
                    patronymic, classVisit, id, ...args}){
        super(args);
        this.purposeOfTheVisit = purposeOfTheVisit;
        this.briefDescriptionOfTheVisit = briefDescriptionOfTheVisit;
        this.urgency = urgency;
        this.age = age;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.classVisit = classVisit;
        this.id = id;
    }

    render(){
        const elem = super.render() ;

        const commentProps = {
            type: "text",
            name: "comment",
            placeholder: "Фио",
            labelText: "ФИО"
        };
        const purposeProps = {
            type:"text",
            name:"comment",
            placeholder:"Цель визита",
            labelText: "Цель визита"

        };
        const briefProps = {
            type: "text",
            name:"comment",
            placeholder: "Краткая цель визита",
            labelText:"Краткая цель визита"
        };
        const urgentProps = {
            name: "priority",
            label:"Срочность визита",
            option:[{value:"normal", text:"Обычная"}, {value:"prior", text:"Приоитетная"}, {value:"urgent", text:"Неотложная"}]
        };
        const comment = new Input(commentProps);
        const purpose = new Input(purposeProps);
        const brief = new Textarea(briefProps);
        const urgent = new Select(urgentProps);

        this.elem.append(comment.render(), purpose.render(), brief.render(), urgent.render());
        elem.className = this.className;
        elem.id = this.id;
        this.elem.addEventListener("submit", this.handleSubmit.bind(this));
        return this.elem;
    }

    handleSubmit(e) {
        e.preventDefault();
        const body = this.serializeJSON();
        if(this.elem.querySelector("[type=hidden]")){
            this.updateVisit(body)
        }else{
            this.addVisit(body)
        }
    }


    async addVisit(){

    }

    async updateVisit() {

    }
}

export default VisitForm;