const render = (...args) => {
    const container = args.pop();
    const argsElements = args.map(elem => elem.render());
    container.append(...argsElements);
}


export {render};