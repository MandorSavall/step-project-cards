import {Modal} from "./modal.js";
import {LoginForm} from "../forms/index.js";
import {render} from "../../functions/index.js";

class LoginModal extends Modal {
    constructor({id, form, modalHeading}){
        super({id, modalHeading});
        this.formProps = Object.assign(form);
        if(this.formProps.functionsAfter){
            this.formProps.functionsAfter.push({
                func: this.close.bind(this),
                args: []
            })
        }
        else {
            this.formProps.functionsAfter = [{
                func: this.close.bind(this),
                args: []
            }]
        }
    }

    render(){
        this.elem = super.render();
        const loginForm = new LoginForm(this.formProps);
        const modalBody = this.elem.querySelector(".modal-body");
        render(loginForm, modalBody);
        
        return this.elem;
    }
}

export {LoginModal};