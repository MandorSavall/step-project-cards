import {createElement} from "../../../functions/index.js";

class Select {
    constructor({name, id, options, className = ""}) {
        className =  `form-control ${className}`;
        this.attr = {name, id, className};
        this.options = [...options];
    }

    render() {
        this.elem = createElement("select", this.attr);
        const options = this.options.map(({value, text}) => `<option value=${value}>${text}</option>`).join("");
        this.elem.insertAdjacentHTML("beforeend", options);
        return this.elem;

    }
}

export {Select};