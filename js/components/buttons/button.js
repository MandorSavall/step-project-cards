import {createElement} from "../../functions/index.js";

class Button {
    constructor({className = "", id, text}) {
        this.attr = {className: `btn ${className}`, id};
        this.text = text;
        this.elem = null;
    }

    render(){
        this.elem = createElement("button", this.attr);
        this.elem.textContent = this.text;
        return this.elem;
    }
}

export {Button};