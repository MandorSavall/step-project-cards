import {Modal} from "./modal.js";
import {SelectDoctorForm} from "../forms/index.js";
import {render} from "../../functions/index.js";

class AddVisitModal extends Modal {
    constructor({id, form}){
        super({id});
        this.formProps = Object.assign(form);
    }

    render(){
        this.elem = super.render();
        const selectDoctorForm = new SelectDoctorForm(this.formProps);
        const modalBody = this.elem.querySelector(".modal-body");
        render(selectDoctorForm, modalBody);
        
        return this.elem;
    }
}

export {AddVisitModal};