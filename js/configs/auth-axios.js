import {siteConfig} from "./site-config.js";
const token = localStorage.getItem("token");

const authReq = axios.create({
    baseURL: siteConfig.baseURL,
    headers: {
        Authorization: `Bearer ${token}`
    }
});

export {authReq};