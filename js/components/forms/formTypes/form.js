import {createElement} from "../../../functions/index.js";

class Form {
    constructor({id, displayType, className = ""}){
        if(displayType === "inline") {
            className+= " form-inline";
        }
        this.attr = {id, className};
    }
    
    render(){
        const elem = createElement("form", this.attr);
        return elem;
    }
    
    serializeJSON(){
        const body = {};
        const eachField = [...this.elem.querySelectorAll('input[name], select[name], textarea[name]')];
        const notEmptyFields = eachField.filter(({value}) => value);
        notEmptyFields.forEach(({name, value}) => body[name] = value);
        return body;
    }
}

export default Form;