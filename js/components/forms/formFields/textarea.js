import {createElement} from "../../../functions/index.js";

class Textarea {
    constructor({value = "", required = false, placeholder = "", name, maxLength, id, text, label, className = ""}){
        className =  `form-control ${className}`;
        this.attr = {value, required, name, maxLength, id, className};
        this.label = label;
        this.text = text;
        this.elem = null;
    }
    
    render(){
        const textareaContainer = createElement("div", {className:"form-group" });
        if (this.label){
            const labelFor = (this.attr.id) ? `for="id"` : "";
            const label = `<label ${labelFor}>Ваш email</label>`;
            inputContainer.insertAdjacentHTML("afterbegin", label)
        }        
        this.elem = createElement("textarea", this.attr);
        this.elem.textContent = this.text
        textareaContainer.append(this.elem);
        return textareaContainer;
    }
}

export {Textarea};