import {Button, LoginModal, AddVisitModal, VisitCardio} from "./components/index.js";
import {render} from "./functions/index.js";

const root = document.getElementById("root");

const loginButtonProps = {
    className: "btn-outline-primary",
    id: "open-login-modal",
    text: 'Авторизация'
};

const btnLogin = new Button(loginButtonProps);
const navbarContent = document.getElementById("navbar-content");
const createVisitButtonProps = {
    className: "btn-outline-primary",
    id: "create-visit-modal",
    text: 'Создать'
};

const btnCreateVisit = new Button(createVisitButtonProps);
render(btnLogin, btnCreateVisit, navbarContent);
btnCreateVisit.elem.style.display = "none";

const functionsAfterLogin = [{
    func: function(){
        btnCreateVisit.elem.style.display = "inline-block";
        btnLogin.elem.remove();
    },
    args: []
}]

const loginModalProps = {
    id: "login-modal",
    modalHeading: 'Авторизация',
    text: 'Авторизация',
    form: {id: "login-form", className: "login-form-modal", functionsAfter: functionsAfterLogin}
};

const loginModal = new LoginModal(loginModalProps);

const addVisitModalProps = {
    id: "login-modal",
    modalHeading: 'Авторизация',
    form: {id: "create-visit-form"}
};

const addVisitModal = new AddVisitModal(addVisitModalProps);

render(loginModal, addVisitModal, root);

btnLogin.elem.addEventListener("click", loginModal.open.bind(loginModal));

btnCreateVisit.elem.addEventListener("click", addVisitModal.open.bind(addVisitModal));
