import {createElement} from "../../../functions/index.js";

class Input {
    constructor({type = "text", id = "", name, placeholder = "", value = "", required = false, className = "", label}) {
        className =  `form-control ${className}`;
        this.attr = {type, id, name, placeholder, value, required, className};
        this.label = label;
        this.elem = null;
    }

    render(){
        const inputContainer = createElement("div", {className: "form-group"});
        if (this.label){
            const labelFor = (this.attr.id) ? `for="id"` : "";
            const label = `<label ${labelFor}>Ваш email</label>`;
            inputContainer.insertAdjacentHTML("afterbegin", label)
        }
        this.elem = createElement('input', this.attr);
        inputContainer.append(this.elem);
        return inputContainer
    }
}

export {Input}