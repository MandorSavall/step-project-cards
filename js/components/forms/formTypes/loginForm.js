import Form from "./form.js";
import {Input} from "../formFields/index.js";
import {render} from "../../../functions/index.js";
import {siteConfig} from "../../../configs/index.js";

class LoginForm extends Form {
    constructor({id, displayType = "", className = "", functionsAfter}) {
        super({id, displayType, className});
        this.functionsAfter = (functionsAfter) ? [...functionsAfter] : null;
    }
    
    render(){
        this.elem = super.render();

        const emailProps = {
            type: "email",
            name: "email",
            placeholder: "Введите ваш email",
            labelText: "Email"
        };
        const email = new Input(emailProps);
        
        const passwordProps = {
            type: "password",
            name: "password",
            placeholder: "Введите ваш пароль",
            labelText: "Password"
        };
        const password = new Input(passwordProps);        

        const submitProps = {
            type: "submit",
            value: 'Отправить'
        };
        const buttonSubmit = new Input(submitProps);

        render(email, password, buttonSubmit, this.elem);
        this.elem.addEventListener("submit", this.handleSubmit.bind(this));
        return this.elem;
        
    }
    
    async handleSubmit(e){
        e.preventDefault();
        const body = this.serializeJSON();
        const {data} = await axios.post(`${siteConfig.baseURL}/login`, body);
        if(data.status === "Success") {
            localStorage.setItem("token", data.token);
            const {functionsAfter} = this;
            if(functionsAfter && functionsAfter.length) {

                functionsAfter.forEach(({func, args}) => func(...args));
            }
        }
        else {
            this.elem.insertAdjacentHTML("aftereend", `<p class="error">Логин или пароль неправильные, проверьте корректность введенных данных пожалуйста</p>`);
        }
    }
}

export {LoginForm};