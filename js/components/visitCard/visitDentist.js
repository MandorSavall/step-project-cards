import Visit from "./visit.js";

class VisitDentist extends Visit {
    constructor(dateOfLastVisit,...args){
        super(args);
        this.doctorType = 'Стоматолог';
        this.dateOfLastVisit = dateOfLastVisit;
    }
    
    render(){
        super.render();
        super.render();
        this.elem.insertAdjacentHTML('afterbegin', `<div class="card-header">
            <h4 class="my-0 font-weight-normal">${this.doctorType}</h4>
            </div>`);
        return this.elem
    }
    showMore(e) {
        super.showMore(e);
        const list = this.elem.querySelector('.adition-info');
        list.insertAdjacentHTML('beforeend', `<li>Дата последнего посещения: ${this.dateOfLastVisit}</li>`)
    }
}

export {VisitDentist};