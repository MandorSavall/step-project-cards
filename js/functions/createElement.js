function createElement(elemName, attributes){
    const newElem = document.createElement(elemName);
    for (const [key, value] of Object.entries(attributes)){
        if (value){
            newElem[key]=value;
        }
    }
    return newElem

}
export {createElement}